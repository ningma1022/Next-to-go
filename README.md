# Getting Started with Next to go

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you should run in below sequence:

### `npm install`

To install all the dependencies.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### `npm test` (optional)

Launches the test runner in the interactive watch mode.

## Some notes

1. This project has tried it's best to implement all the technical tasks listed in the "Entain Technical Task" PDF.

   - See 5 races at all times
   - Sorted by time ascending
   - Race disappear from the list after 1 min past the start time and a new race will be pushed up to make it 5 races
   - User can see meeting name at race number and a countdown timer
   - User can toggle to see the selected category (by default, all the categories are enabled)

2. I have added some comments in the code, I try to explain the code, also tried to note what could we do alternately if it's a real project.

3. This project has added some unit tests for demonstration purpose but it is not thoroughly unit tested.

4. To see the SPA, you have to run the project locally due to the Neds API has implemented CORS policy.
