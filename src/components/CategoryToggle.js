import React, { useState } from 'react';
import './CategoryToggle.css';

const CategoryToggle = ({ name, toggleCategory }) => {
  const [toggle, setToggle] = useState(true);

  const onToggleCategory = () => {
    toggleCategory(!toggle, name);
    setToggle(!toggle);
  };

  return (
    <div className="toggle">
      <p className="toggle-name">{name}</p>
      <label className="toggle-switch">
        <input
          type="checkbox"
          value={name}
          checked={toggle}
          onChange={onToggleCategory}
        />
        <span className="toggle-slider"></span>
      </label>
    </div>
  );
};

export default CategoryToggle;
