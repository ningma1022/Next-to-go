import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';

import CategoryToggle from './CategoryToggle';

let container = null;
beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

test('should simulate user toggles category', () => {
  const toggleCategory = jest.fn();

  render(
    <CategoryToggle name="abc" toggleCategory={toggleCategory} />,
    container
  );

  const button = container.querySelector('input');

  act(() => {
    button.dispatchEvent(new MouseEvent('click', { bubbles: true }));
  });

  expect(toggleCategory).toHaveBeenCalledTimes(1);
  expect(toggleCategory).toHaveBeenCalledWith(false, 'abc');

  // when user clicks again, should dispatch true
  act(() => {
    button.dispatchEvent(new MouseEvent('click', { bubbles: true }));
  });
  expect(toggleCategory).toHaveBeenCalledWith(true, 'abc');
});
