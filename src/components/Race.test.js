import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import Race from './Race';

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement('div');
  document.body.appendChild(container);
  jest.useFakeTimers();
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
  jest.useRealTimers();
});

test('should run the timer as expected', () => {
  const replaceRace = jest.fn();
  const race = {
    advertised_start: {
      seconds: 1000000,
    },
  };
  render(<Race race={race} replaceRace={replaceRace} />, container);

  // move ahead in time by 100ms, shouldn't call replaceRace
  act(() => {
    jest.advanceTimersByTime(100);
  });
  expect(replaceRace).not.toHaveBeenCalled();

  // Move ahead by 3 seconds, should call replaceRace 3 times
  act(() => {
    jest.advanceTimersByTime(3000);
  });
  expect(replaceRace).toHaveBeenCalledTimes(3);
});

test('renders the correct text', () => {
  const replaceRace = jest.fn();
  const race = {
    advertised_start: {
      seconds: 1000000,
    },
    race_number: '100',
    meeting_name: 'hello',
  };

  render(<Race race={race} replaceRace={replaceRace} />, container);

  const meetingName = screen.getByText('hello');
  expect(meetingName).toBeInTheDocument();

  const raceNumber = screen.getByText(/100/);
  expect(raceNumber).toBeInTheDocument();
});
