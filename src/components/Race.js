import React, { useState, useEffect } from 'react';
import { categoryMap } from '../App';
import './Race.css';

const Race = ({ race, replaceRace }) => {
  const { seconds } = race.advertised_start;
  const [secondsDiff, setSecondsDiff] = useState(() =>
    Math.floor(new Date().getTime() / 1000 - seconds)
  );

  useEffect(() => {
    // Setup the countdown timer
    const timerId = setTimeout(() => {
      const diff = Math.floor(new Date().getTime() / 1000 - seconds);
      if (diff > 60) {
        replaceRace(race.race_id);
      }
      setSecondsDiff(diff);
    }, 1000);
    return () => {
      clearTimeout(timerId);
    };
  }, [secondsDiff, seconds, race.race_id, replaceRace]);

  /**
   * @returns A string telling users the race starts in/has started for: (x minutes) x seconds.
   */
  const getCountdownInfo = () => {
    const prefix = secondsDiff < 0 ? 'Starts in:' : 'Has started for:';
    const absDiff = Math.abs(secondsDiff);

    if (absDiff > 59) {
      const minutes = Math.floor(absDiff / 60);
      const seconds = absDiff - minutes * 60;
      return `${prefix} ${minutes} minute${
        minutes === 1 ? '' : 's'
      } ${seconds} second${seconds === 1 ? '' : 's'}`;
    }

    return `${prefix} ${absDiff} second${absDiff === 1 ? '' : 's'}`;
  };

  return (
    <div className="race">
      <p>
        {categoryMap.get(race.category_id)} @{' '}
        <span className="race-meeting">{race.meeting_name}</span>
      </p>
      <p>Race number: {race.race_number}</p>
      <p>{getCountdownInfo()}</p>
    </div>
  );
};

export default Race;
