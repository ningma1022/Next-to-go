import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import App from './App';
import * as axios from 'axios';
jest.mock('axios');

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

test('when having good response', async () => {
  const mockData = {
    data: {
      data: {
        race_summaries: {
          a: {
            race_id: 'abc',
            category_id: '9daef0d7-bf3c-4f50-921d-8e818c60fe61',
            advertised_start: {
              seconds: 10000,
            },
          },
        },
      },
    },
  };

  jest.spyOn(axios, 'get').mockImplementation(() => Promise.resolve(mockData));

  // eslint-disable-next-line testing-library/no-unnecessary-act
  await act(async () => {
    render(<App />, container);
  });

  expect(container.querySelector('h1')).toHaveTextContent('Next to go');
  expect(container.querySelector('p')).toHaveTextContent('Greyhound racing');
});

test('when having bad response', async () => {
  jest
    .spyOn(axios, 'get')
    .mockImplementation(() => Promise.resolve({ bad_data: 'bad' }));

  // eslint-disable-next-line testing-library/no-unnecessary-act
  await act(async () => {
    render(<App />, container);
  });

  expect(container.querySelector('h1')).toHaveTextContent(
    'Whoops, something went wrong, please refresh the page.'
  );
});
