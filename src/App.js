import { useEffect, useState } from 'react';
import CategoryToggle from './components/CategoryToggle';
import Race from './components/Race';
import './App.css';
import axios from 'axios';

/**
 * Define a category map so that it can be reused and also makes the code a bit more robust
 */
export const categoryMap = new Map([
  ['9daef0d7-bf3c-4f50-921d-8e818c60fe61', 'Greyhound racing'],
  ['161d9be2-e909-4326-8c2c-35ed71fb460b', 'Harness racing'],
  ['4a2788f8-e825-4d36-9894-efd4baf1cfae', 'Horse racing'],
]);

/**
 * I put some business code in the App component, but in real project,
 * the App component shouldn't be the place of these business code,
 * the only thing the App component should do is auth, configure store (if use redux) etc.
 */
function App() {
  const [races, setRaces] = useState([]);
  const [displayRaces, setDisplayRaces] = useState([]);
  const [error, setError] = useState(false);
  const [disabledCategories, setDisabledCategories] = useState([]);

  useEffect(() => {
    (async () => {
      await initSetup();
    })();
  }, []);

  /**
   * Initial setup to fetch data from BE, setup states.
   * I didn't use a state management library in this project, but if we have one,
   * we would have a central place to handle all the HTTP related things, making code a bit more decoupled
   */
  const initSetup = async () => {
    try {
      const { data } = await axios.get(
        'https://api.neds.com.au/rest/v1/racing/',
        {
          params: {
            method: 'nextraces',
            count: 10,
          },
        }
      );

      const races = Object.values(data.data.race_summaries).sort(
        (a, b) => a.advertised_start.seconds - b.advertised_start.seconds
      );

      setRaces(races);
      // The display races should be always an array of 5.
      setDisplayRaces(races.slice(0, 5));
    } catch (error) {
      // Basic error handling.
      // We need to use status code (200, 404, 500, etc) for more refined error handling in real project if needed
      setError(true);
    }
  };

  /**
   * @param {boolean} toggled - check if user toggles to show or hide a category
   * @param {string} name - the name of the toggled category
   * We only want show the races that not belong to the currently disabled categories.
   * To make it as much as 5 races, we will pull the races from invisible races to the displayRaces.
   */
  const onToggleCategory = (toggled, name) => {
    let currentDisabled = disabledCategories;
    const categoryId = [...categoryMap].find(([key, val]) => val === name)[0];

    if (!toggled) {
      currentDisabled.push(categoryId);
    } else {
      currentDisabled = currentDisabled.filter((c) => c !== categoryId);
    }

    const filteredRaces = races
      .filter((race) => !currentDisabled.includes(race.category_id))
      .slice(0, 5);

    setDisplayRaces(filteredRaces);
    setDisabledCategories(currentDisabled);
  };

  /**
   * @param {string} raceId - the race id
   * This function is called when a race has past for over 1 minute.
   * If total races is less than 5 after filtering, we need to fetch the race again,
   * because we want to show 5 races all the time according to the requirement.
   * When having filters applied, we want to filter out the races belong to the disabled category
   */
  const onReplaceRace = (raceId) => {
    const newRaces = races.filter((race) => race.race_id !== raceId);

    if (newRaces.length <= 5) {
      initSetup();
      return;
    }

    setRaces(newRaces);
    setDisplayRaces(
      newRaces
        .filter((race) => !disabledCategories.includes(race.category_id))
        .slice(0, 5)
    );
  };

  /**
   * Get the category from Map then push all the categories in the Map to an array
   * @returns An array of category toggles
   */
  const getCategories = () => {
    const categories = [];
    categoryMap.forEach((category) => {
      categories.push(
        <CategoryToggle
          key={category}
          name={category}
          toggleCategory={onToggleCategory}
        />
      );
    });

    return categories;
  };

  return (
    <div className="container">
      {error ? (
        <h1 className="error">
          Whoops, something went wrong, please refresh the page.
        </h1>
      ) : (
        <>
          <h1 className="title">Next to go</h1>
          <div className="container-toggle">{getCategories()}</div>
          <div className="container-race">
            {displayRaces.map((race) => (
              <Race
                key={race.race_id}
                race={race}
                replaceRace={onReplaceRace}
              />
            ))}
          </div>
        </>
      )}
    </div>
  );
}

export default App;
